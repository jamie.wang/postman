FROM node:10.9.0-alpine

ENV NEWMAN_VERSION 4.1.0
RUN npm install -g newman@${NEWMAN_VERSION};
RUN npm install newman-reporter-html;
RUN apk add --no-cache apache2-utils #ab

WORKDIR /etc/newman
CMD ["newman"]